package com.app.zapateria.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAppZapateriaRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAppZapateriaRestApplication.class, args);
	}

}
