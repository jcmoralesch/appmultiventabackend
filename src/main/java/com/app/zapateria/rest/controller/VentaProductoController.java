package com.app.zapateria.rest.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Usuario;
import com.app.zapateria.rest.model.entity.VentaProducto;
import com.app.zapateria.rest.model.service.ICantidadProductoService;
import com.app.zapateria.rest.model.service.IUsuarioService;
import com.app.zapateria.rest.model.service.IVentaProductoService;

@RestController
@CrossOrigin(origins = {"http:localhost:4200","*"})
@RequestMapping("/api")
public class VentaProductoController {
	
	@Autowired
	private IVentaProductoService ventaProductoService;
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private ICantidadProductoService cantidadProductoService;
	
	@PostMapping("/venta")
	private ResponseEntity<?> store(@Valid @RequestBody VentaProducto ventaProducto,
			                       BindingResult errores,OAuth2Authentication autenthication){
		
		Map<String, Object> response= new  HashMap<>();
		VentaProducto ventaProductoNew= new VentaProducto();
		Usuario usuario=usuarioService.findByUsername(autenthication.getName());
		CantidadProducto cantidadProducto=cantidadProductoService.getByProductoAndAgencia(
				                                         ventaProducto.getProducto().getCodigo(),usuario.getPersonal().getAgencia().getNombre());
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());;
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		Double desc=ventaProducto.getProducto().getPrecioVenta()-ventaProducto.getPrecioVenta();
		Double gan=ventaProducto.getPrecioVenta()-ventaProducto.getProducto().getPrecio();
		
		ventaProducto.setUsuario(usuario);
		ventaProducto.setDescuento(desc);
		ventaProducto.setGanancia(gan);
		cantidadProducto.setCantidad(cantidadProducto.getCantidad()-1);
		
		try {
			
			ventaProductoNew=ventaProductoService.store(ventaProducto,cantidadProducto);
		}
		catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("venta",ventaProductoNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
	}
	
	@GetMapping("/venta/{fecha1}/{fecha2}/{agencia}")
	private List<VentaProducto> getByFechaBetween(@PathVariable String fecha1,@PathVariable String fecha2,
			                                      @PathVariable String agencia){
		DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		LocalDate date1 = LocalDate.parse(fecha1, inputFormatter);
		LocalDate date2 = LocalDate.parse(fecha2,inputFormatter);
		
		if(agencia.equals("undefined")) {
			agencia="";
		}
		
		if(agencia=="") {
			return ventaProductoService.getByFechaVentaBetween(date1, date2);
		}
		
		return ventaProductoService.getByFechaventaAndAgencia(date1, date2, agencia);	
	}
	
	@GetMapping("/venta")
	private List<VentaProducto> getAllByAgencia(OAuth2Authentication authentication){
		Usuario usuario= usuarioService.findByUsername(authentication.getName());
		LocalDateTime fh = LocalDateTime.now().minusHours(6);	
		LocalDate fechaActual =  LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth()); //Se calcula fecha de la siguiente manera por diferencia de zona horaria en docker
		
		return ventaProductoService.getByAgenciaAndCurrentDate(usuario.getPersonal().getAgencia().getNombre(), fechaActual);
	}

}
