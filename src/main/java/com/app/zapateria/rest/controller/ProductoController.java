package com.app.zapateria.rest.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.app.zapateria.rest.model.entity.Producto;
import com.app.zapateria.rest.model.service.IProductoService;

@RestController
@CrossOrigin({ "http://localhost:4200", "*" })
@RequestMapping("/api")
public class ProductoController {

	@Autowired
	private IProductoService productoService;
	private final Logger log = org.slf4j.LoggerFactory.getLogger(ProductoController.class);
	// private final AWSCredentials credentials = new BasicAWSCredentials("AKIAS7LTP5OL7UTUBKY4",
			// "/H75GGlKpoMQHVgUmfDhzzn/w3IDFEjFmRsecATI");
	// private final AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
			// .withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1).build();
	private String nombreBucket="imagneness3publico";  

	@PostMapping("/producto")
	private ResponseEntity<?> store(@Valid @RequestBody Producto producto, BindingResult errores) {

		Map<String, Object> response = new HashMap<>();
		Producto productoNew = new Producto();

		if (errores.hasErrors()) {
			List<String> errors = errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			producto.setCodigo(producto.getCodigo().toUpperCase());
			producto.setNombre(producto.getNombre().toUpperCase());
			productoNew = productoService.store(producto);

		} catch (DataIntegrityViolationException ex) {
			response.put("mensaje", "Error al registrar");
			response.put("err", "El código: ".concat(producto.getCodigo()).concat(" Ya esta registrada"));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("producto", productoNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@GetMapping("/producto")
	private List<Producto> getAll() {
		return productoService.getAll();
	}

	@PostMapping("/producto/upload")
	private ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
		Map<String, Object> response = new HashMap<>();
		Producto producto = productoService.findById(id);
		
		String nombreArchivo = "";
		if (!archivo.isEmpty()) {

			try {
				nombreArchivo = enviarFotoS3(archivo);
			} catch (IOException e) {
				response.put("mensaje", "Error al subir la imagen al servidor");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			String nombreFotoAnterior = producto.getFoto();

			if (nombreFotoAnterior != null && nombreFotoAnterior.length() > 0) {// Borrar foto
				/* try {
					s3Client.getObject(nombreBucket, nombreFotoAnterior);
					try {
						s3Client.deleteObject(nombreBucket, producto.getFoto());
					} catch (AmazonServiceException e) {
						System.err.println(e.getErrorMessage());
					}
				} catch (AmazonServiceException e) {
					System.err.println(e.getErrorMessage());
				} */
			}

			producto.setFoto(nombreArchivo);
			productoService.store(producto);

			response.put("mensaje", "archivo cargado correctamente");
			response.put("producto", producto);
		}

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	private String enviarFotoS3(MultipartFile mpFile) throws IOException {
		String nombreArchivo = UUID.randomUUID().toString() + "_" + mpFile.getOriginalFilename().replace(" ", "");
		byte[] bytes = mpFile.getBytes();
		/* try {
			InputStream stream = new ByteArrayInputStream(bytes);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(bytes.length);

			s3Client.putObject(new PutObjectRequest(nombreBucket, nombreArchivo, stream, metadata)
					.withCannedAcl(CannedAccessControlList.PublicRead));

			return nombreArchivo;
		} catch (AmazonServiceException ase) {
			System.err.println("Errores de amazon" + ase.getErrorMessage());
		} catch (AmazonClientException ace) {
			System.err.println("Error Message " + ace.getMessage());
		}*/

		return null;
	}

	@GetMapping("/producto/img/{nombreFoto:.+}")
	private ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto) {
		Resource recurso = null;
		// S3Object s3o = s3Client.getObject(nombreBucket, nombreFoto);

		/* try {
			URL url = new URL("https://s3.amazonaws.com/"+nombreBucket+"/" + s3o.getKey());
			recurso = new UrlResource(url);
		} catch (MalformedURLException e1) {
			System.err.println(e1.getMessage());
			e1.printStackTrace();
		} */

		if (!recurso.exists() && !recurso.isReadable()) {
			try {
				URL urlAlternativo = new URL("https://s3.amazonaws.com/"+nombreBucket+"/img2.png");
				recurso = new UrlResource(urlAlternativo);
			} catch (MalformedURLException e) {

				e.printStackTrace();
			}
			log.error("Error no se pudo cargar la imagen: " + nombreFoto);
		}

		// para forzar la descarga

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");

		return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
	}

	@GetMapping("/producto/{id}")
	private Producto getById(@PathVariable Long id) {
		return productoService.findById(id);
	}

	@PutMapping("/producto/{id}")
	private ResponseEntity<?> update(@Valid @PathVariable Long id, @RequestBody Producto producto,
			BindingResult errores) {
		Map<String, Object> response = new HashMap<>();
		Producto obtenerProducto = productoService.findById(id);
		Producto productoActualizado = new Producto();

		if (errores.hasErrors()) {
			List<String> errors = errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			obtenerProducto.setCodigo(producto.getCodigo().toUpperCase());
			obtenerProducto.setNombre(producto.getNombre().toUpperCase());
			obtenerProducto.setPrecio(producto.getPrecio());
			obtenerProducto.setPrecioVenta(producto.getPrecioVenta());
			obtenerProducto.setPrecioDocena(producto.getPrecioDocena());
			obtenerProducto.setPrecioMayor(producto.getPrecioMayor());
			obtenerProducto.setCategoria(producto.getCategoria());
			obtenerProducto.setAlerta(producto.getAlerta());
			obtenerProducto.setMarca(producto.getMarca());
			obtenerProducto.setProveedor(producto.getProveedor());
			productoActualizado = productoService.store(obtenerProducto);
		} catch (DataIntegrityViolationException ex) {
			response.put("mensaje", "Error al actualizar");
			response.put("err", "Esta ingresando un codigo ya registrada");

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la actualización en  BBDD");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("producto", productoActualizado);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
}
