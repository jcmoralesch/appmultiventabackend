package com.app.zapateria.rest.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.IngresoProducto;
import com.app.zapateria.rest.model.entity.Usuario;
import com.app.zapateria.rest.model.service.IIngresoProductoService;
import com.app.zapateria.rest.model.service.IUsuarioService;

@RestController
@CrossOrigin(origins = {"http://localhost:4200","*"})
@RequestMapping("/api")
public class IngresoProductoController {
	
	@Autowired
	private IIngresoProductoService ingresoProductoService;
	@Autowired
	private IUsuarioService usuarioService;
	
	@PostMapping("/ingreso-producto")
	private ResponseEntity<?> store(@Valid @RequestBody IngresoProducto ingresoProducto, 
			                   BindingResult errores, OAuth2Authentication autentication ){
		Map<String, Object> response = new HashMap<>();
		IngresoProducto ingresoProductoNew= new IngresoProducto();
		Usuario usuario=usuarioService.findByUsername(autentication.getName());
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		Double total=ingresoProducto.getCantidad()*ingresoProducto.getProducto().getPrecio();
		
		ingresoProducto.setUsuario(usuario);
		ingresoProducto.setTotal(total);
		ingresoProducto.setTraslado(ingresoProducto.getCantidad());
		ingresoProducto.setPrecio(ingresoProducto.getProducto().getPrecio());
		
		try {
		  	ingresoProductoNew=ingresoProductoService.store(ingresoProducto);
		}
		catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("ingresoProducto",ingresoProductoNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/ingreso-producto")
	private List<IngresoProducto> getAll(){
		return ingresoProductoService.findByTrasladoGreaterThan(0);
	}
	
	
	@GetMapping("/ingreso-producto/{fecha1}/{fecha2}")
	private List<IngresoProducto> findByFechaIngresoBeetween(@PathVariable String fecha1,@PathVariable String fecha2){
		
		DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		LocalDate date1 = LocalDate.parse(fecha1, inputFormatter);
		LocalDate date2 = LocalDate.parse(fecha2,inputFormatter);
	    
		return ingresoProductoService.findByFechaIngresoBetween(date1, date2);
		
	}

}
