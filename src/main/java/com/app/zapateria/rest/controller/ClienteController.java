package com.app.zapateria.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.Cliente;
import com.app.zapateria.rest.model.service.IClienteService;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
@RequestMapping("/api")
public class ClienteController {
	
	@Autowired
	private IClienteService clienteService;
	
	@PostMapping("/cliente")
	private ResponseEntity<?> store(@Valid @RequestBody Cliente cliente,BindingResult errores){
		Map<String, Object> response = new HashMap<>();
		Cliente clienteNew = new Cliente();
		
		if(errores.hasErrors()) {
			List<String> errors =null;
			
			response.put("errors",errors);
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		cliente.setNombre(cliente.getNombre().toUpperCase());
		cliente.setApellido(cliente.getApellido().toUpperCase());
		cliente.setDireccion(cliente.getDireccion().toUpperCase());
		
		try {
			
			clienteNew=clienteService.store(cliente);
		}
		catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("cliente",clienteNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/cliente/filtrar-cliente/{term}")
	@ResponseStatus(HttpStatus.OK)
	private List<Cliente> filterByNobreOrApellido(@PathVariable String term){
		return clienteService.findByNombreOrApellido(term);
	}

}
