package com.app.zapateria.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.Personal;
import com.app.zapateria.rest.model.service.IPersonalService;

@RestController
@CrossOrigin({"http://localhost:4200","*"})
@RequestMapping("/api")
public class PersonalController {
	
	@Autowired
	private IPersonalService personalService;
	
	@PostMapping("/personal")
	private ResponseEntity<?> store(@Valid @RequestBody Personal personal,BindingResult errores){
		
		Personal personalNew= new Personal();
		Map<String, Object> response = new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			personal.setIdentificacion(personal.getIdentificacion().toUpperCase());
			personal.setNombre(personal.getNombre().toUpperCase());
			personal.setApellido(personal.getApellido().toUpperCase());
			personal.setDireccion(personal.getDireccion().toUpperCase());
			
		
			personalNew=personalService.store(personal);
			
		}
		catch(DataIntegrityViolationException ex) {
			response.put("mensaje", "Error al registrar");
			response.put("err","Esta ingresando una identificacion ya registrada".concat(personal.getIdentificacion()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(DataAccessException e) {
			response.put("mensaje","Error al realizar el insert en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","Registro ingresada con éxito");
		response.put("personal", personalNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}
	
	@GetMapping("/personal/{page}")
	private Page<Personal>findAll(@PathVariable Integer page){
		return personalService.getAll(PageRequest.of(page, 10));
	}
	
	@GetMapping("/personal/personal/{id}")
	private Personal findById(@PathVariable Long id) {
		return personalService.finById(id);
	}
	
	@PutMapping("/personal/update/{id}")
	private ResponseEntity<?> update(@Valid @PathVariable Long id, @RequestBody Personal personal,BindingResult errores){
		
		Personal personalFinded=personalService.finById(id);
		Personal personalUpdated= new Personal();
		
		Map<String, Object>response= new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String>errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		personalFinded.setNombre(personal.getNombre().toUpperCase());
		personalFinded.setApellido(personal.getApellido().toUpperCase());
		personalFinded.setDireccion(personal.getDireccion().toUpperCase());
		personalFinded.setAgencia(personal.getAgencia());
		personalFinded.setTelefono(personal.getTelefono());
		personalFinded.setIdentificacion(personal.getIdentificacion());
			
		try {
			personalUpdated=personalService.store(personalFinded);
		}catch(DataIntegrityViolationException ex) {
			response.put("mensaje", "Error al actualizar");
			response.put("err","Esta ingresando una identificacion ya registrada".concat(personal.getIdentificacion()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(DataAccessException e) {
			response.put("mensaje","Error al realizar la actualización en  BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","Registro actualizado con éxito");
		response.put("personal", personalUpdated);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
	}
	
	@DeleteMapping("/personal/delete/{id}")
	private ResponseEntity<?> delete(@PathVariable Long id){
		
		Personal personalDelete=personalService.finById(id);
		Map<String,Object> response= new HashMap<>();		
		try {			
			personalService.delete(personalDelete);		
		}
		catch(DataAccessException e) {
			
			response.put("mensaje", "No se puede eliminar");
			response.put("err", personalDelete.getNombre().concat(" Tiene usuario activo"));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		
		response.put("mensaje","Registro eliminado correctamente");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NO_CONTENT);
		
	}

}
