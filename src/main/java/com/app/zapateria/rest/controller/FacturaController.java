package com.app.zapateria.rest.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Factura;
import com.app.zapateria.rest.model.entity.ItemFactura;
import com.app.zapateria.rest.model.entity.Usuario;
import com.app.zapateria.rest.model.service.ICantidadProductoService;
import com.app.zapateria.rest.model.service.IFacturaService;
import com.app.zapateria.rest.model.service.IUsuarioService;



@RestController
@CrossOrigin(origins = {"http://localhost:4200","*"})
@RequestMapping("/api")
public class FacturaController {
	
	@Autowired
	private IFacturaService facturaService;
	@Autowired
	private ICantidadProductoService cantidadProductoService;
	@Autowired
	private IUsuarioService usuarioService;
	
	@GetMapping("/factura/filtrar-productos/{term}")
	@ResponseStatus(HttpStatus.OK)
	private List<CantidadProducto> filtrarProductos(@PathVariable String term,OAuth2Authentication autenthication){
		Usuario usuario=usuarioService.findByUsername(autenthication.getName());
		
		return  cantidadProductoService.findProductoFilterByNombre(term,usuario.getPersonal().getAgencia().getNombre());
	}
	
	@PostMapping("/factura")
	@ResponseStatus(HttpStatus.CREATED)
	private Factura crear(@RequestBody Factura factura, OAuth2Authentication autenthication) {
		
		Usuario usuario=usuarioService.findByUsername(autenthication.getName());
	    CantidadProducto cantidadProducto=null;		
		List<ItemFactura>it=factura.getItems();
		List<ItemFactura>pv=new ArrayList<ItemFactura>();
		
		for (ItemFactura itemFactura : it) {
			
			cantidadProducto=cantidadProductoService.findByAgenciaAndProducto(usuario.getPersonal().getAgencia(),
					itemFactura.getProducto().getProducto());
			
			cantidadProducto.setCantidad(cantidadProducto.getCantidad()-itemFactura.getCantidad());
			
			cantidadProductoService.store(cantidadProducto);
			
			if(itemFactura.getPrecioVenta()==0) {
			     itemFactura.setPrecioVenta(cantidadProducto.getProducto().getPrecioVenta());
			}else {
				itemFactura.setPrecioVenta(itemFactura.getPrecioVenta());
			}
			
			pv.add(itemFactura);
			
		}
		
		factura.setItems(pv);
		factura.setUsuario(usuario);
		
		return facturaService.store(factura);
	}
	
	@GetMapping("/factura")
	private List<Factura> getAll(OAuth2Authentication authentication){
		Usuario usuario= usuarioService.findByUsername(authentication.getName());
		return facturaService.getByFechaOrderByDes(usuario.getPersonal().getAgencia().getNombre());	
	}
	
	@GetMapping("/factura/current-date")
	private List<Factura> getByCurrentDate(){
		
		LocalDateTime fh = LocalDateTime.now().minusHours(6);	
		LocalDate fechaActual =  LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth()); //Se calcula fecha de la siguiente manera por diferencia de zona horaria en docker
		return facturaService.findAllByCurrentDate(fechaActual);
	}
	
	@GetMapping("/factura/get-by-date/{fecha1}/{fecha2}/{agencia}")
	private List<Factura> finByFechaBetween(@PathVariable String fecha1, @PathVariable String fecha2, @PathVariable String agencia){
		DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		LocalDate date1 = LocalDate.parse(fecha1, inputFormatter);
		LocalDate date2 = LocalDate.parse(fecha2,inputFormatter);
		
		if(agencia.equals("undefined")) {
			agencia="";
		}
		
		if(agencia=="") {
			return facturaService.findByFechaRegistroBetween(date1, date2);
		}
		
		return facturaService.findByFechaRegistroBetweenAndAgencia(date1, date2, agencia);	
	}
}
