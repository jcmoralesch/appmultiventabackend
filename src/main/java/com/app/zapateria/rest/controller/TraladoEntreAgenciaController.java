package com.app.zapateria.rest.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.TrasladoEntreAgencia;
import com.app.zapateria.rest.model.entity.Usuario;
import com.app.zapateria.rest.model.service.ICantidadProductoService;
import com.app.zapateria.rest.model.service.ITrasladoEntreAgenciaService;
import com.app.zapateria.rest.model.service.UsuarioService;

@RestController
@CrossOrigin(origins = { "http://localhost:4200", "*" })
@RequestMapping("/api")
public class TraladoEntreAgenciaController {

	@Autowired
	private ITrasladoEntreAgenciaService trasladoEntreAgenciaService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private ICantidadProductoService cantidadProductoService;

	@PostMapping("/traslado-entre-agencia")
	private ResponseEntity<?> store(@Valid @RequestBody TrasladoEntreAgencia trasladoEntreAgencia,
			BindingResult errores, OAuth2Authentication autentication) {
		Map<String, Object> response = new HashMap<>();
		TrasladoEntreAgencia trasladoEntreAgenciaNew = new TrasladoEntreAgencia();
		Usuario usuario = usuarioService.findByUsername(autentication.getName());
		CantidadProducto agenciaDestino=null;
		Integer restarCantidad=0;

		if (errores.hasErrors()) {
			List<String> errors = errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
             trasladoEntreAgencia.setUsuario(usuario);
     		CantidadProducto agenciaOrigen = cantidadProductoService.findByAgenciaAndProducto(
     				trasladoEntreAgencia.getCantidadProducto().getAgencia(),
     				trasladoEntreAgencia.getCantidadProducto().getProducto());
     		agenciaDestino = cantidadProductoService.findByAgenciaAndProducto(
     				trasladoEntreAgencia.getAgenciaDestino(), trasladoEntreAgencia.getCantidadProducto().getProducto());
     		
     		restarCantidad = agenciaOrigen.getCantidad() - trasladoEntreAgencia.getCantidad();
     		
     		if(agenciaDestino==null) {
     			
     			agenciaDestino = new CantidadProducto();
     			
     			agenciaDestino.setAgencia(trasladoEntreAgencia.getAgenciaDestino());
     			agenciaDestino.setCantidad(trasladoEntreAgencia.getCantidad());
     			agenciaDestino.setProducto(trasladoEntreAgencia.getCantidadProducto().getProducto());
     		}else {
     			restarCantidad = agenciaOrigen.getCantidad() - trasladoEntreAgencia.getCantidad();
         		Integer sumarCantidad = agenciaDestino.getCantidad() + trasladoEntreAgencia.getCantidad();
         		agenciaDestino.setCantidad(sumarCantidad);
     		}
     		
     		agenciaOrigen.setCantidad(restarCantidad);
			trasladoEntreAgenciaNew = trasladoEntreAgenciaService.store(trasladoEntreAgencia, agenciaOrigen,
					agenciaDestino);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (NullPointerException e) {
			response.put("mensaje", "Error No existe aun productos en esta agencia");
			response.put("err", e.getMessage().concat(": ").concat(e.getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} 

		response.put("trasladoEntreAgencia", trasladoEntreAgenciaNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@GetMapping("/traslado-entre-agencia/{fecha1}/{fecha2}")
	private List<TrasladoEntreAgencia> getByDate(@PathVariable String fecha1,@PathVariable String fecha2){
		
		DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		LocalDate date1 = LocalDate.parse(fecha1, inputFormatter);
		LocalDate date2 = LocalDate.parse(fecha2,inputFormatter);
		
		return trasladoEntreAgenciaService.getByDate(date1, date2);
	}

}
