package com.app.zapateria.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.service.IAgenciaService;
import com.app.zapateria.rest.model.service.ICantidadProductoService;

@RestController
@CrossOrigin(origins = {"http://localhost:4200" ,"*"})
@RequestMapping("/api")
public class CantidadProductoController {
	
	@Autowired
	private ICantidadProductoService cantidadProductoService;
	@Autowired
	private IAgenciaService agenciaService;
	
	@PostMapping("/cantidad-producto")
	private  ResponseEntity<?> store(@Valid @RequestBody CantidadProducto cantidadProducto,
			                        BindingResult errores,OAuth2Authentication autentication){
		Map<String, Object> response= new HashMap<>();
		CantidadProducto cantidadProductoNew= new CantidadProducto();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			cantidadProductoNew = cantidadProductoService.store(cantidadProducto);
		}catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("cantidadProducto", cantidadProductoNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/cantidad-producto/{nombre}")
	private List<CantidadProducto> getAll(@PathVariable String nombre){
		
		return cantidadProductoService.findByAgencia(agenciaService.findByNombre(nombre));
	}
	
	@GetMapping("/cantidad-producto/agencia/cant/{agencia}")
	private List<CantidadProducto> getByAngenciaAndCantidad(@PathVariable String agencia){
		return cantidadProductoService.getProductoByAgenciaAndCantidad(agencia);
	}
	
	@GetMapping("/cantidad-producto")
	private List<CantidadProducto> getAll(){
		return cantidadProductoService.getAll();
	}
	
	@GetMapping("/cantidad-producto/cantidad/{cantidad}")
	private List<CantidadProducto> getCantidadAll(@PathVariable Integer cantidad){
		return cantidadProductoService.getByCantidadAll(cantidad);
	}
	
	@GetMapping("/cantidad-producto/{agencia}/{cantidad}")
	private List<CantidadProducto> getByAgencianAndCantidad(@PathVariable Integer cantidad,@PathVariable String agencia){
		return cantidadProductoService.getByAgenciaAndCantidad(agencia, cantidad);
	}
	
	
	
	@GetMapping("/cantidad-producto/filtrar/{marca}/{agencia}/{categoria}")
	private List<CantidadProducto> getProductoByFilter(@PathVariable String marca, @PathVariable String agencia, @PathVariable String categoria){
		
		
		if(marca.equals("undefined")) {
			marca="";
		}
		if(categoria.equals("undefined")) {
			categoria="";
		}
       
       if(categoria.equals("") && marca!="") {
    	   return cantidadProductoService.getByMarca(agencia,marca);
       }
       
       if(marca.equals("") && categoria!="") {
    	   return cantidadProductoService.getByAgenciaCategoria(agencia, categoria);
       }
       
       if(marca!="" && categoria!="") {
    	   return cantidadProductoService.getByAgenciaMarcaCategoria(agencia, marca, categoria);
       }
       
      
      
        return null;		
	}

}
