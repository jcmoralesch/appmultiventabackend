package com.app.zapateria.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.Agencia;
import com.app.zapateria.rest.model.service.IAgenciaService;

@CrossOrigin(origins = {"http://localhost:4200","*"})
@RestController
@RequestMapping("/api")
public class AgenciaController {
	
	@Autowired
	private IAgenciaService agenciaService;
	
	@PostMapping("/agencia")
	private ResponseEntity<?> store(@Valid  @RequestBody Agencia agencia,BindingResult errores){
		
		Agencia agenciaNew = new Agencia();
		Map<String, Object> response = new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
		     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			agencia.setNombre(agencia.getNombre().toUpperCase());
			agencia.setUbicacion(agencia.getUbicacion().toUpperCase());
			agenciaNew = agenciaService.store(agencia);
			
		}
		catch(DataIntegrityViolationException ex) {
			response.put("mensaje", "Error al registrar");
			response.put("err", "El nombre: ".concat( agencia.getNombre()).concat(" Ya esta registrada"));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "Agencia registrada con éxito");
		response.put("agencia", agenciaNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);		
	}
	
	@GetMapping("/agencia")
	private List<Agencia> findAll(){
		return agenciaService.findAll();
	}
	
	@GetMapping("/agencia/cantidad-producto")
	private List<Agencia> findAllInCantidadProducto(){
		return agenciaService.findAgenciaInCantidadProducto();
	}
	
	
}
