package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.Role;

public interface IRoleService {
	
	public List<Role> getAll();

}
