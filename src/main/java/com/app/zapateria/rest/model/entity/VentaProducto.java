package com.app.zapateria.rest.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "venta_productos")
public class VentaProducto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@DecimalMin(value = "0.00")
	@NotNull
	private Double precioVenta;
	private Double descuento;
	private Double ganancia;
	@Column(name = "fecha_venta")
	private LocalDate fechaVenta;
	@Column(name = "hora_venta")
	private LocalTime horaVenta;
	@ManyToOne
	private Producto producto;
	@ManyToOne
	private Usuario usuario;

	@PrePersist
	private void prePrersist() {
		// fechaVenta = LocalDate.now(ZoneId.of("America/Guatemala"));
		// horaVenta = LocalTime.now(ZoneId.of("America/Guatemala"));

		// *************Se calcula fecha y hora de las siguiente manera por diferencia de zona horaria en docker*********************************
		LocalDateTime fh = LocalDateTime.now().minusHours(6);
		fechaVenta = LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth());
		horaVenta = LocalTime.of(fh.getHour(), fh.getMinute(), fh.getSecond(), fh.getNano());
		// **************************************************************************************************************************************
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public Double getGanancia() {
		return ganancia;
	}

	public void setGanancia(Double ganancia) {
		this.ganancia = ganancia;
	}

	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(LocalDate fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public LocalTime getHoraVenta() {
		return horaVenta;
	}

	public void setHoraVenta(LocalTime horaVenta) {
		this.horaVenta = horaVenta;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
