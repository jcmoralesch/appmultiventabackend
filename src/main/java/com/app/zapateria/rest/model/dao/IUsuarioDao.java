package com.app.zapateria.rest.model.dao;

import org.springframework.data.repository.CrudRepository;
import com.app.zapateria.rest.model.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {
	
	public Usuario findByUsername(String username);

}
