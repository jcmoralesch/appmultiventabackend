package com.app.zapateria.rest.model.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.zapateria.rest.model.entity.Agencia;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Producto;

public interface ICantidadProductoDao extends JpaRepository<CantidadProducto, Long>  {
	
	public CantidadProducto findByAgenciaAndProducto(Agencia agencia,Producto producto);
	public List<CantidadProducto> findByAgencia(Agencia agencia);
	@Query("select p from CantidadProducto p where p.producto.codigo=?1 and p.agencia.nombre=?2")
	public CantidadProducto findByProductoAndAgencia(String producto,String agencia);
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.cantidad=?2")
	public List<CantidadProducto> findByAgenciaAndCantidad(String agencia,Integer cantidad);
	@Query("select p from CantidadProducto p where p.cantidad=?1")
	public List<CantidadProducto> findByCantidadAll(Integer cantidad);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.cantidad>0")
	public List<CantidadProducto> findProductoByAgenciaAndCantidad(String nombre);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.cantidad>0")
	public List<CantidadProducto> findByMarca(String agencia,String marca);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.categoria.categoria=?2")
	public List<CantidadProducto> findByAgenciaCategoria(String agencia,String categoria);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.producto.categoria.categoria=?3")
	public List<CantidadProducto> findByAgenciaMarcaCategoria(String agencia,String marca,String categoria);
	
	@Query("select p from CantidadProducto p where (p.producto.nombre like %?1% or p.producto.codigo like %?1%) and p.agencia.nombre=?2 and p.cantidad>0 ")
	public List<CantidadProducto> findProductoFilterByNombre(String nombre,String agencia);
	
}
