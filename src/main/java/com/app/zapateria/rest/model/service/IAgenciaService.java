package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.Agencia;

public interface IAgenciaService {
	
	public Agencia store(Agencia agencia);
	public List<Agencia> findAll();
	public Agencia findByNombre(String nombre);
	public List<Agencia> findAgenciaInCantidadProducto();

}
