package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.Empresa;

public interface IEmpresaDao extends JpaRepository<Empresa, Long> {

}
