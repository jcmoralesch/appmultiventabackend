package com.app.zapateria.rest.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.Proveedor;

public interface IProveedorDao extends JpaRepository<Proveedor, Long> {
	
	public Page<Proveedor> findByStatus(String status,Pageable pageable);
	public List<Proveedor> findByStatus(String status);

}
