package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.TrasladoEntreAgencia;

public interface ITrasladoEntreAgenciaService {
	
	public TrasladoEntreAgencia store(TrasladoEntreAgencia trasladoEntreAgencia,CantidadProducto agenciaOrigen,CantidadProducto agenciaDestino);
	public List<TrasladoEntreAgencia> getByDate(LocalDate fecha1,LocalDate fecha2);

}
