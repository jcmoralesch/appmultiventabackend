package com.app.zapateria.rest.model.service;

import java.util.List;

import com.app.zapateria.rest.model.entity.Categoria;

public interface ICategoriaService {
	
	public Categoria store(Categoria categoria);
	public List<Categoria> findAll();

}
