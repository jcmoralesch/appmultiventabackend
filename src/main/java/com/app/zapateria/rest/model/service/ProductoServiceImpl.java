package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.zapateria.rest.model.dao.IProductoDao;
import com.app.zapateria.rest.model.entity.Producto;

@Service
public class ProductoServiceImpl implements IProductoService{
	
	@Autowired
	private IProductoDao productoDao;

	@Override
	public Producto store(Producto producto) {
		
		return productoDao.save(producto);
	}

	@Override
	public Producto findById(Long id) {
		
		return productoDao.findById(id).orElse(null);
	}

	@Override
	public List<Producto> getAll() {
		
		return productoDao.findAllOrderByIdDesc();
	}

	@Override
	public List<Producto> findByNombreContainingIgnoreCase(String term) {
		
		return productoDao.findByNombreContainingIgnoreCase(term);
	}

}
