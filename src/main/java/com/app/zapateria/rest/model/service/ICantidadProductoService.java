package com.app.zapateria.rest.model.service;

import java.util.List;

import com.app.zapateria.rest.model.entity.Agencia;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Producto;

public interface ICantidadProductoService {
	
	public CantidadProducto store(CantidadProducto cantidadProducto);
	public CantidadProducto findByAgenciaAndProducto(Agencia agencia,Producto producto);
	public CantidadProducto getByProductoAndAgencia(String producto,String agencia);
	public List<CantidadProducto> getAll();
	public List<CantidadProducto> getByCantidadAll(Integer cantidad);
	public List<CantidadProducto> getByAgenciaAndCantidad(String agencia,Integer cantidad);
	public List<CantidadProducto> findByAgencia(Agencia agencia);
	public List<CantidadProducto> getProductoByAgenciaAndCantidad(String nombre);
	public List<CantidadProducto> getByMarca(String agencia,String marca);
	public List<CantidadProducto> getByAgenciaCategoria(String agencia,String categoria);
	public List<CantidadProducto> getByAgenciaMarcaCategoria(String agencia,String marca,String categoria);	
	public List<CantidadProducto> findProductoFilterByNombre(String nombre,String agencia);
    
}
