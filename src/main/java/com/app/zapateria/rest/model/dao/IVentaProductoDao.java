package com.app.zapateria.rest.model.dao;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.zapateria.rest.model.entity.VentaProducto;

public interface IVentaProductoDao extends JpaRepository<VentaProducto, Long> {
	
	public List<VentaProducto> findByFechaVentaBetween(LocalDate fecha1,LocalDate fecha2);
	@Query("select v from VentaProducto v where v.fechaVenta between ?1 and ?2 and v.usuario.personal.agencia.nombre=?3")
	public List<VentaProducto> findByFechaventaAndAgencia(LocalDate fecha1,LocalDate fecha2,String agencia);
	
	@Query("select v from VentaProducto v where v.usuario.personal.agencia.nombre=?1 and v.fechaVenta=?2")
	public List<VentaProducto> findByAgenciaAndCurrentDate(String agencia, LocalDate fecha);
	

}
