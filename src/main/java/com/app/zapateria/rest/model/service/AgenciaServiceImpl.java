package com.app.zapateria.rest.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.IAgenciaDao;
import com.app.zapateria.rest.model.entity.Agencia;

@Service
public class AgenciaServiceImpl implements IAgenciaService{
	
	@Autowired
	private IAgenciaDao agenciaDao;

	@Override
	public Agencia store(Agencia agencia) {
		
		return agenciaDao.save(agencia);
	}

	@Override
	public List<Agencia> findAll() {
		
		return agenciaDao.findAll();
	}

	@Override
	public Agencia findByNombre(String nombre) {
		
		return agenciaDao.findByNombre(nombre);
	}

	@Override
	public List<Agencia> findAgenciaInCantidadProducto() {
		
		return agenciaDao.findAgenciaInCantidadProducto();
	}

}
