package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.zapateria.rest.model.dao.IEmpresaDao;
import com.app.zapateria.rest.model.dao.IProveedorDao;
import com.app.zapateria.rest.model.entity.Empresa;
import com.app.zapateria.rest.model.entity.Proveedor;

@Service
public class ProveedorServiceImpl implements IProveedorService{
	
	@Autowired
	private IProveedorDao proveedorDao;
	
	@Autowired
	private IEmpresaDao empresaDao;
    
	@Transactional
	@Override
	public Proveedor store(Proveedor proveedor, Empresa empresa) {
		 empresaDao.save(empresa);
		
		return proveedorDao.save(proveedor);
	}

	@Override
	public Page<Proveedor> getAllByStatus(String status, Pageable pageable) {
		
		return proveedorDao.findByStatus(status, pageable);
	}

	@Override
	public Proveedor getById(Long id) {
		
		return proveedorDao.findById(id).orElse(null);
	}

	@Override
	public Proveedor update(Proveedor proveedor) {
		
		return proveedorDao.save(proveedor);
	}

	@Override
	public List<Proveedor> getAllByStatus(String status) {
		
		return proveedorDao.findByStatus(status);
	}

}
