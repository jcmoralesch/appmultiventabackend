package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.Cliente;

public interface IClienteService {
	
	public Cliente store(Cliente cliente);
	public List<Cliente> findByNombreOrApellido(String term);

}
