package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.ICantidadProductoDao;
import com.app.zapateria.rest.model.entity.Agencia;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Producto;

@Service
public class CantidadProductoServiceImpl implements ICantidadProductoService {
	
	@Autowired
	private ICantidadProductoDao cantidadProductoDao;

	@Override
	public CantidadProducto store(CantidadProducto cantidadProducto) {
		
		return cantidadProductoDao.save(cantidadProducto);
	}
	

	@Override
	public List<CantidadProducto> getAll() {
		return cantidadProductoDao.findAll();
	}

	@Override
	public CantidadProducto findByAgenciaAndProducto(Agencia agencia, Producto producto)  throws NullPointerException{
		
		return cantidadProductoDao.findByAgenciaAndProducto(agencia, producto);
	}
	
	@Override
	public List<CantidadProducto> getByAgenciaAndCantidad(String agencia, Integer cantidad) {
		
		return cantidadProductoDao.findByAgenciaAndCantidad(agencia, cantidad);
	}
	
	@Override
	public List<CantidadProducto> getByCantidadAll(Integer cantidad) {
		
		return cantidadProductoDao.findByCantidadAll(cantidad);
	}
	
	@Override
	public CantidadProducto getByProductoAndAgencia(String producto,String agencia) {
	     
		return cantidadProductoDao.findByProductoAndAgencia(producto,agencia);
	}

	@Override
	public List<CantidadProducto> findByAgencia(Agencia agencia) {
		
		return cantidadProductoDao.findByAgencia(agencia);
	}

	
	@Override
	public List<CantidadProducto> getProductoByAgenciaAndCantidad(String nombre) {
		
		return cantidadProductoDao.findProductoByAgenciaAndCantidad(nombre);
	}

	@Override
	public List<CantidadProducto> getByMarca(String agencia, String marca) {
		
		return cantidadProductoDao.findByMarca(agencia, marca);
	}

	@Override
	public List<CantidadProducto> getByAgenciaCategoria(String agencia, String categoria) {
		
		return cantidadProductoDao.findByAgenciaCategoria(agencia, categoria);
	}

	@Override
	public List<CantidadProducto> getByAgenciaMarcaCategoria(String agencia, String marca, String categoria) {
		
		return cantidadProductoDao.findByAgenciaMarcaCategoria(agencia, marca, categoria);
	}


	@Override
	public List<CantidadProducto> findProductoFilterByNombre(String nombre,String agencia) {
		
		return cantidadProductoDao.findProductoFilterByNombre(nombre,agencia);
	}

}
