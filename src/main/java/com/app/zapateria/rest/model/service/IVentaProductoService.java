package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.VentaProducto;

public interface IVentaProductoService {
	
	public VentaProducto store(VentaProducto ventaProducto,CantidadProducto cantidadProducto);
	public List<VentaProducto> getByFechaVentaBetween(LocalDate fecha1,LocalDate fecha2);
	public List<VentaProducto> getByFechaventaAndAgencia(LocalDate fecha1,LocalDate fecha2,String agencia);
	public List<VentaProducto> getByAgenciaAndCurrentDate(String agencia, LocalDate fecha);

}
