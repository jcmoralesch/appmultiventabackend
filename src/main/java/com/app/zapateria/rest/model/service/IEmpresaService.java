package com.app.zapateria.rest.model.service;

import com.app.zapateria.rest.model.entity.Empresa;

public interface IEmpresaService {
	
	public Empresa store(Empresa empresa);

}
