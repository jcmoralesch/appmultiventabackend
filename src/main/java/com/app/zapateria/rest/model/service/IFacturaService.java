package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;
import com.app.zapateria.rest.model.entity.Factura;

public interface IFacturaService {
	
	public Factura store(Factura factura);
	public List<Factura> getByFechaOrderByDes(String agencia);
	public List<Factura> findAllByCurrentDate(LocalDate fechaActual);
	public List<Factura> findByFechaRegistroBetween(LocalDate fecha1, LocalDate fecha2);
	public List<Factura> findByFechaRegistroBetweenAndAgencia(LocalDate fecha1, LocalDate fecha2, String agencia);
	
}
