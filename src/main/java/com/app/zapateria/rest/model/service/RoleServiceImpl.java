package com.app.zapateria.rest.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.IRoleDao;
import com.app.zapateria.rest.model.entity.Role;

@Service
public class RoleServiceImpl implements IRoleService{
	
	@Autowired
	private IRoleDao roleDao;

	@Override
	public List<Role> getAll() {
		
		return (List<Role>) roleDao.findAll();
	}

}
