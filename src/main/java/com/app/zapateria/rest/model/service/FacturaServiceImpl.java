package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.zapateria.rest.model.dao.IFacturaDao;
import com.app.zapateria.rest.model.entity.Factura;

@Service
public class FacturaServiceImpl implements IFacturaService{
	
	@Autowired
	private IFacturaDao facturaDao;

	@Override
	@Transactional
	public Factura store(Factura factura) {
		return facturaDao.save(factura);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> getByFechaOrderByDes(String agencia) {
	
		return facturaDao.findByFechaOrderByDes(agencia, LocalDate.now(ZoneId.of("America/Guatemala")));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> findAllByCurrentDate(LocalDate fechaActual) {

		return facturaDao.findAllByCurrentDate(fechaActual);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> findByFechaRegistroBetween(LocalDate fecha1, LocalDate fecha2) {
		return facturaDao.findByFechaRegistroBetween(fecha1, fecha2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> findByFechaRegistroBetweenAndAgencia(LocalDate fecha1, LocalDate fecha2, String agencia) {
		
		return facturaDao.findByFechaRegistroBetweenAndAgencia(fecha1, fecha2, agencia);
	}
}
