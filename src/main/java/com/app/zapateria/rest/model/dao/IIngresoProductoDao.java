package com.app.zapateria.rest.model.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.IngresoProducto;

public interface IIngresoProductoDao extends JpaRepository<IngresoProducto, Long> {
	
	public List<IngresoProducto> findByTrasladoGreaterThan(Integer traslado);
	public List<IngresoProducto> findByFechaIngresoBetween(LocalDate fecha1,LocalDate fecha2);

}
