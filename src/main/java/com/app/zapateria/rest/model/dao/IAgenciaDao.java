package com.app.zapateria.rest.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.zapateria.rest.model.entity.Agencia;

public interface IAgenciaDao extends JpaRepository<Agencia, Long> {
	
	public Agencia findByNombre(String nombre);
	@Query("select distinct p.agencia from CantidadProducto p")
	public List<Agencia> findAgenciaInCantidadProducto();

}
