package com.app.zapateria.rest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.IPersonalDao;
import com.app.zapateria.rest.model.entity.Personal;

@Service
public class PersonalServiceImpl implements IPersonalService{
	
	@Autowired
	private IPersonalDao personalDao;

	@Override
	public Personal store(Personal personal) {
		
		return personalDao.save(personal);
	}

	@Override
	public Page<Personal> getAll(Pageable pageable) {
		
		return personalDao.findAll(pageable);
	}

	@Override
	public Personal finById(Long id) {
		
		return personalDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Personal personal) {
		 personalDao.delete(personal);	
	}

	

}
