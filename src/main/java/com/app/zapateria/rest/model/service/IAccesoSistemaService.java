package com.app.zapateria.rest.model.service;

import com.app.zapateria.rest.model.entity.AccesoSistema;

public interface IAccesoSistemaService {
	
	public AccesoSistema store(AccesoSistema accesoSistema);

}
