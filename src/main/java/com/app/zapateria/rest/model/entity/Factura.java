package com.app.zapateria.rest.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "facturas")
public class Factura implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private LocalDate fechaRegistro;
	private LocalTime horaRegistro;
	@ManyToOne
	private Usuario usuario;

	@JsonIgnoreProperties(value = { "facturas", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	private Cliente cliente;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "items" })
	@JoinColumn(name = "factura_id")
	private List<ItemFactura> items;

	@PrePersist
	private void prePersist() {
		// this.fechaRegistro = LocalDate.now(ZoneId.of("America/Guatemala"));
		// this.horaRegistro = LocalTime.now(ZoneId.of("America/Guatemala"));
		
		// *************Se calcula fecha y hora de las siguiente manera por diferencia de zona horaria en docker*********************************
		LocalDateTime fh = LocalDateTime.now().minusHours(6);
		this.fechaRegistro = LocalDate.of(fh.getYear(), fh.getMonthValue(), fh.getDayOfMonth());
		this.horaRegistro = LocalTime.of(fh.getHour(), fh.getMinute(), fh.getSecond(), fh.getNano());
		// **************************************************************************************************************************************
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDate fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public LocalTime getHoraRegistro() {
		return horaRegistro;
	}

	public void setHoraRegistro(LocalTime horaRegistro) {
		this.horaRegistro = horaRegistro;
	}

	public List<ItemFactura> getItems() {
		return items;
	}

	public void setItems(List<ItemFactura> items) {
		this.items = items;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Double getTotal() {
		Double total = 0.00;
		for (ItemFactura item : items) {
			total += item.getImporte();
		}

		return total;
	}

}
