package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.zapateria.rest.model.dao.ICantidadProductoDao;
import com.app.zapateria.rest.model.dao.IVentaProductoDao;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.VentaProducto;

@Service
public class VentaProductoServiceImpl implements IVentaProductoService{
	
	@Autowired
	private IVentaProductoDao ventaProductoDao;
	@Autowired
	private ICantidadProductoDao cantidadProductoDao;

	@Override
	@Transactional
	public VentaProducto store(VentaProducto ventaProducto,CantidadProducto cantidadProducto) {
		
		cantidadProductoDao.save(cantidadProducto);
		return ventaProductoDao.save(ventaProducto);
	}

	@Override
	public List<VentaProducto> getByFechaVentaBetween(LocalDate fecha1, LocalDate fecha2) {
		
		return ventaProductoDao.findByFechaVentaBetween(fecha1, fecha2);
	}

	@Override
	public List<VentaProducto> getByFechaventaAndAgencia(LocalDate fecha1, LocalDate fecha2, String agencia) {
		
		return ventaProductoDao.findByFechaventaAndAgencia(fecha1, fecha2, agencia);
	}

	@Override
	public List<VentaProducto> getByAgenciaAndCurrentDate(String agencia, LocalDate fecha) {
		
		return ventaProductoDao.findByAgenciaAndCurrentDate(agencia, fecha);
	}

}
