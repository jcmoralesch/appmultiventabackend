package com.app.zapateria.rest.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.zapateria.rest.model.entity.Cliente;

public interface IClienteDao extends JpaRepository<Cliente, Long> {
	
	@Query("select c from Cliente c where (c.nombre like %?1% or c.apellido like %?1%)")
	public List<Cliente> findByNombreOrApellido(String term);

}
