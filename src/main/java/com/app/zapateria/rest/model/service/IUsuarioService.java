package com.app.zapateria.rest.model.service;

import java.util.List;

import com.app.zapateria.rest.model.entity.Usuario;


public interface IUsuarioService {
	
	public Usuario findByUsername(String username);
	public Usuario store(Usuario usuario);
	public Usuario findById(Long id);
	public List<Usuario> findAll();
	public void delete(Usuario usuario);

}
