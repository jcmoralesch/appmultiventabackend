package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.zapateria.rest.model.dao.ICategoriaDao;
import com.app.zapateria.rest.model.entity.Categoria;

@Service
public class CategoriaServiceImpl  implements ICategoriaService{
	
	@Autowired
	private ICategoriaDao categoriaDao;

	@Override
	public Categoria store(Categoria categoria) {

		return categoriaDao.save(categoria);
	}

	@Override
	public List<Categoria> findAll() {
		
		return categoriaDao.findAll();
	}

}
