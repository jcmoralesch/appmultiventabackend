package com.app.zapateria.rest.model.dao;

import org.springframework.data.repository.CrudRepository;
import com.app.zapateria.rest.model.entity.AccesoSistema;

public interface IAccesoSistemaDao extends CrudRepository<AccesoSistema, Long> {
	
	

}
