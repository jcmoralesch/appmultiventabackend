package com.app.zapateria.rest.model.dao;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.zapateria.rest.model.entity.Factura;

public interface IFacturaDao extends JpaRepository<Factura, Long> {
	
	@Query("select f from Factura f where f.usuario.personal.agencia.nombre=?1 and f.fechaRegistro=?2 order by f.id desc")
    public List<Factura> findByFechaOrderByDes(String agencia, LocalDate fechaActual);
	@Query("select f from Factura f where f.fechaRegistro=?1")
	public List<Factura> findAllByCurrentDate(LocalDate fechaActual);
	public List<Factura> findByFechaRegistroBetween(LocalDate fecha1, LocalDate fecha2);
	@Query("select f from Factura f where f.fechaRegistro between ?1 and ?2 and f.usuario.personal.agencia.nombre=?3")
	public List<Factura> findByFechaRegistroBetweenAndAgencia(LocalDate fecha1, LocalDate fecha2, String agencia);
}
