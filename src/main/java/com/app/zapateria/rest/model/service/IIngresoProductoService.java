package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;

import com.app.zapateria.rest.model.entity.IngresoProducto;

public interface IIngresoProductoService {
	
	public IngresoProducto store(IngresoProducto ingresoProducto);
	public List<IngresoProducto> getAll();
	public List<IngresoProducto> findByTrasladoGreaterThan(Integer traslado);
	public IngresoProducto findById(Long id);
	public List<IngresoProducto> findByFechaIngresoBetween(LocalDate fecha1,LocalDate fecha2);

}
