package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.app.zapateria.rest.model.entity.Empresa;
import com.app.zapateria.rest.model.entity.Proveedor;

public interface IProveedorService {
	
	public Proveedor store(Proveedor proveedor,Empresa empresa);
	public Page<Proveedor> getAllByStatus(String status,Pageable pageable);
	public Proveedor getById(Long id);
	public Proveedor update(Proveedor proveedor);
	public List<Proveedor> getAllByStatus(String status);
	

}
