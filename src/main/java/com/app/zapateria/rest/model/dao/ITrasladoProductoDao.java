package com.app.zapateria.rest.model.dao;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.TrasladoProducto;

public interface ITrasladoProductoDao extends JpaRepository<TrasladoProducto, Long> {
	
	public List<TrasladoProducto> findByFechaBetween(LocalDate fecha1,LocalDate fecha2);

}
