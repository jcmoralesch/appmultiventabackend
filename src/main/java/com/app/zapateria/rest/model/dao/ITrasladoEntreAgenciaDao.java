package com.app.zapateria.rest.model.dao;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.TrasladoEntreAgencia;

public interface ITrasladoEntreAgenciaDao extends JpaRepository<TrasladoEntreAgencia, Long> {
	
	public List<TrasladoEntreAgencia> findByFechaBetween(LocalDate fecha1,LocalDate fecha2);

}
