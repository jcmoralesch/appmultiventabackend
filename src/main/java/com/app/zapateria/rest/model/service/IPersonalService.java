package com.app.zapateria.rest.model.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.app.zapateria.rest.model.entity.Personal;

public interface IPersonalService {
	
	public Personal store(Personal personal);
	public Page<Personal> getAll(Pageable pageable);
	public Personal finById(Long id);
	public void delete(Personal personal);
	

}
