package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.zapateria.rest.model.dao.ICantidadProductoDao;
import com.app.zapateria.rest.model.dao.ITrasladoEntreAgenciaDao;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.TrasladoEntreAgencia;

@Service
public class TrasladoEntreAgenciaServiceImpl implements ITrasladoEntreAgenciaService{
	
	@Autowired
	private ITrasladoEntreAgenciaDao trasladoEntreAgenciaDao;
	@Autowired
	private ICantidadProductoDao cantidadProductoDao;

	@Override
	@Transactional
	public TrasladoEntreAgencia store(TrasladoEntreAgencia trasladoEntreAgencia,CantidadProducto agenciaOrigen,CantidadProducto agenciaDestino) {
		cantidadProductoDao.save(agenciaOrigen);
		cantidadProductoDao.save(agenciaDestino);
		return trasladoEntreAgenciaDao.save(trasladoEntreAgencia);
	}

	@Override
	public List<TrasladoEntreAgencia> getByDate(LocalDate fecha1, LocalDate fecha2) {
		
		return trasladoEntreAgenciaDao.findByFechaBetween(fecha1, fecha2);
	}

}
